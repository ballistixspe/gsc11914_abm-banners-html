(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [
		{name:"GSC11914_ABM_banners_300x250_AD1_atlas_", frames: [[0,0,300,250],[0,463,300,194],[0,252,300,209],[0,659,96,37]]}
];


// symbols:



(lib.bg1 = function() {
	this.initialize(ss["GSC11914_ABM_banners_300x250_AD1_atlas_"]);
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.fade = function() {
	this.initialize(ss["GSC11914_ABM_banners_300x250_AD1_atlas_"]);
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.graphic = function() {
	this.initialize(ss["GSC11914_ABM_banners_300x250_AD1_atlas_"]);
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.logo = function() {
	this.initialize(ss["GSC11914_ABM_banners_300x250_AD1_atlas_"]);
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Tween13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.text = new cjs.Text("Power User?", "bold 35px 'Open Sans Extrabold'", "#FFFFFF");
	this.text.lineHeight = 48;
	this.text.lineWidth = 232;
	this.text.parent = this;
	this.text.setTransform(-115.9,-23.8);

	this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-117.9,-25.8,235.8,51.7);


(lib.Tween11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.text = new cjs.Text("A CAD", "bold 35px 'Open Sans Extrabold'", "#33CCFF");
	this.text.lineHeight = 48;
	this.text.lineWidth = 138;
	this.text.parent = this;
	this.text.setTransform(-69.1,-23.8);

	this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-71.1,-25.8,142.3,51.7);


(lib.Tween8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.text = new cjs.Text("Are You", "bold 35px 'Open Sans Extrabold'", "#FFFFFF");
	this.text.lineHeight = 48;
	this.text.lineWidth = 152;
	this.text.parent = this;
	this.text.setTransform(-76.05,-23.8);

	this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-78,-25.8,156.1,51.7);


(lib.Tween6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.text = new cjs.Text("Your Engineering Co-Pilot", "15px 'Open Sans'", "#FFFFFF");
	this.text.lineHeight = 21;
	this.text.lineWidth = 135;
	this.text.parent = this;
	this.text.setTransform(-67.65,-30.85);

	this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-69.6,-32.8,139.3,65.69999999999999);


(lib.Scene_1_graphic = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// graphic
	this.instance = new lib.graphic();
	this.instance.parent = this;
	this.instance.setTransform(0,44);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(751));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_fade = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// fade
	this.instance = new lib.fade();
	this.instance.parent = this;
	this.instance.setTransform(0,-8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(751));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_background = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// background
	this.instance = new lib.bg1();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(751));

}).prototype = p = new cjs.MovieClip();


(lib.logo_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.logo();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.logo_Layer_1, null, null);


(lib.power_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween13("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(117.9,25.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.power_Layer_1, null, null);


(lib.power = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.power_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(117.9,25.8,1,1,0,0,0,117.9,25.8);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.power, new cjs.Rectangle(0,0,235.8,51.7), null);


(lib.logo_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.logo_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(48,18.5,1,1,0,0,0,48,18.5);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.logo_1, new cjs.Rectangle(0,0,96,37), null);


(lib.copilot_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween6("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(69.65,32.85);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.copilot_Layer_1, null, null);


(lib.copilot = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.copilot_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(69.7,32.9,1,1,0,0,0,69.7,32.9);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.copilot, new cjs.Rectangle(0,0,139.4,65.8), null);


(lib.are_you_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween8("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(78.05,25.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.are_you_Layer_1, null, null);


(lib.a_cad_Layer_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween11("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(71.1,25.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.a_cad_Layer_1, null, null);


(lib.Scene_1_logo = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// logo
	this.instance = new lib.logo_1();
	this.instance.parent = this;
	this.instance.setTransform(252,212.65,1,1,0,0,0,48,18.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(50).to({_off:false},0).to({x:235.2},50).wait(651));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_Layer_23 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_23
	this.instance = new lib.power();
	this.instance.parent = this;
	this.instance.setTransform(113.3,119.85,1,1,0,0,0,117.9,25.8);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(250).to({_off:false},0).to({x:130.85},50).wait(451));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_copilot = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// copilot
	this.instance = new lib.copilot();
	this.instance.parent = this;
	this.instance.setTransform(70.55,223.8,1,1,0,0,0,69.7,32.9);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(100).to({_off:false},0).to({x:85.55},50).wait(601));

}).prototype = p = new cjs.MovieClip();


(lib.areyou = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.are_you_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(78,25.8,1,1,0,0,0,78,25.8);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.areyou, new cjs.Rectangle(0,0,156.2,51.7), null);


(lib.acad = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1_obj_
	this.Layer_1 = new lib.a_cad_Layer_1();
	this.Layer_1.name = "Layer_1";
	this.Layer_1.parent = this;
	this.Layer_1.setTransform(71.1,25.8,1,1,0,0,0,71.1,25.8);
	this.Layer_1.depth = 0;
	this.Layer_1.isAttachedToCamera = 0
	this.Layer_1.isAttachedToMask = 0
	this.Layer_1.layerDepth = 0
	this.Layer_1.layerIndex = 0
	this.Layer_1.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.acad, new cjs.Rectangle(0,0,142.3,51.7), null);


(lib.Scene_1_are_you = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// are_you
	this.instance = new lib.areyou();
	this.instance.parent = this;
	this.instance.setTransform(77.05,38.75,1,1,0,0,0,78,25.8);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(150).to({_off:false},0).to({x:93.45},50).wait(551));

}).prototype = p = new cjs.MovieClip();


(lib.Scene_1_a_cad = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// a_cad
	this.instance = new lib.acad();
	this.instance.parent = this;
	this.instance.setTransform(71.1,79.7,1,1,0,0,0,71.1,25.8);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(200).to({_off:false},0).to({x:84.05},50).wait(501));

}).prototype = p = new cjs.MovieClip();


// stage content:
(lib.GSC11914_ABMbanners_300x250AD1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	this.___GetDepth___ = function(obj) {
		var depth = obj.depth;
		var cameraObj = this.___camera___instance;
		if(cameraObj && cameraObj.depth && obj.isAttachedToCamera)
		{
			depth += depth + cameraObj.depth;
		}
		return depth;
		}
	this.___needSorting___ = function() {
		for (var i = 0; i < this.getNumChildren() - 1; i++)
		{
			var prevDepth = this.___GetDepth___(this.getChildAt(i));
			var nextDepth = this.___GetDepth___(this.getChildAt(i + 1));
			if (prevDepth < nextDepth)
				return true;
		}
		return false;
	}
	this.___sortFunction___ = function(obj1, obj2) {
		return (this.exportRoot.___GetDepth___(obj2) - this.exportRoot.___GetDepth___(obj1));
	}
	this.on('tick', function (event){
		var curTimeline = event.currentTarget;
		if (curTimeline.___needSorting___()){
			this.sortChildren(curTimeline.___sortFunction___);
		}
	});

	// timeline functions:
	this.frame_750 = function() {
		this.___loopingOver___ = true;
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(750).call(this.frame_750).wait(1));

	// Layer_23_obj_
	this.Layer_23 = new lib.Scene_1_Layer_23();
	this.Layer_23.name = "Layer_23";
	this.Layer_23.parent = this;
	this.Layer_23.depth = 0;
	this.Layer_23.isAttachedToCamera = 0
	this.Layer_23.isAttachedToMask = 0
	this.Layer_23.layerDepth = 0
	this.Layer_23.layerIndex = 0
	this.Layer_23.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.Layer_23).wait(751));

	// a_cad_obj_
	this.a_cad = new lib.Scene_1_a_cad();
	this.a_cad.name = "a_cad";
	this.a_cad.parent = this;
	this.a_cad.depth = 0;
	this.a_cad.isAttachedToCamera = 0
	this.a_cad.isAttachedToMask = 0
	this.a_cad.layerDepth = 0
	this.a_cad.layerIndex = 1
	this.a_cad.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.a_cad).wait(751));

	// are_you_obj_
	this.are_you = new lib.Scene_1_are_you();
	this.are_you.name = "are_you";
	this.are_you.parent = this;
	this.are_you.depth = 0;
	this.are_you.isAttachedToCamera = 0
	this.are_you.isAttachedToMask = 0
	this.are_you.layerDepth = 0
	this.are_you.layerIndex = 2
	this.are_you.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.are_you).wait(751));

	// copilot_obj_
	this.copilot = new lib.Scene_1_copilot();
	this.copilot.name = "copilot";
	this.copilot.parent = this;
	this.copilot.depth = 0;
	this.copilot.isAttachedToCamera = 0
	this.copilot.isAttachedToMask = 0
	this.copilot.layerDepth = 0
	this.copilot.layerIndex = 3
	this.copilot.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.copilot).wait(751));

	// logo_obj_
	this.logo = new lib.Scene_1_logo();
	this.logo.name = "logo";
	this.logo.parent = this;
	this.logo.depth = 0;
	this.logo.isAttachedToCamera = 0
	this.logo.isAttachedToMask = 0
	this.logo.layerDepth = 0
	this.logo.layerIndex = 4
	this.logo.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.logo).wait(751));

	// fade_obj_
	this.fade = new lib.Scene_1_fade();
	this.fade.name = "fade";
	this.fade.parent = this;
	this.fade.setTransform(150,89,1,1,0,0,0,150,89);
	this.fade.depth = 0;
	this.fade.isAttachedToCamera = 0
	this.fade.isAttachedToMask = 0
	this.fade.layerDepth = 0
	this.fade.layerIndex = 5
	this.fade.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.fade).wait(751));

	// graphic_obj_
	this.graphic = new lib.Scene_1_graphic();
	this.graphic.name = "graphic";
	this.graphic.parent = this;
	this.graphic.setTransform(150,148.5,1,1,0,0,0,150,148.5);
	this.graphic.depth = 0;
	this.graphic.isAttachedToCamera = 0
	this.graphic.isAttachedToMask = 0
	this.graphic.layerDepth = 0
	this.graphic.layerIndex = 6
	this.graphic.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.graphic).wait(751));

	// background_obj_
	this.background = new lib.Scene_1_background();
	this.background.name = "background";
	this.background.parent = this;
	this.background.setTransform(150,125,1,1,0,0,0,150,125);
	this.background.depth = 0;
	this.background.isAttachedToCamera = 0
	this.background.isAttachedToMask = 0
	this.background.layerDepth = 0
	this.background.layerIndex = 7
	this.background.maskLayerName = 0

	this.timeline.addTween(cjs.Tween.get(this.background).wait(751));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(145.4,117,154.6,139.7);
// library properties:
lib.properties = {
	id: '3A63D98A80DC44E08CC40C7CA7AB2146',
	width: 300,
	height: 250,
	fps: 50,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/GSC11914_ABM_banners_300x250_AD1_atlas_.png", id:"GSC11914_ABM_banners_300x250_AD1_atlas_"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['3A63D98A80DC44E08CC40C7CA7AB2146'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


// Layer depth API : 

AdobeAn.Layer = new function() {
	this.getLayerZDepth = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth; else 0;";
		return eval(script);
	}
	this.setLayerZDepth = function(timeline, layerName, zDepth)
	{
		const MAX_zDepth = 10000;
		const MIN_zDepth = -5000;
		if(zDepth > MAX_zDepth)
			zDepth = MAX_zDepth;
		else if(zDepth < MIN_zDepth)
			zDepth = MIN_zDepth;
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline." + layerName + ".depth = " + zDepth + ";";
		eval(script);
	}
	this.removeLayer = function(timeline, layerName)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		var script = "if(timeline." + layerName + ") timeline.removeChild(timeline." + layerName + ");";
		eval(script);
	}
	this.addNewLayer = function(timeline, layerName, zDepth)
	{
		if(layerName === "Camera")
		layerName = "___camera___instance";
		zDepth = typeof zDepth !== 'undefined' ? zDepth : 0;
		var layer = new createjs.MovieClip();
		layer.name = layerName;
		layer.depth = zDepth;
		layer.layerIndex = 0;
		timeline.addChild(layer);
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;